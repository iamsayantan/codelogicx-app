import React from 'react';
import { Platform } from 'react-native';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';

export default TabNavigator(
  {
    Dashboard: {
      screen: HomeScreen,
    },
    Worklogs: {
      screen: LinksScreen,
    },
    Leaves: {
      screen: SettingsScreen,
    },
    More: {
      screen: SettingsScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Dashboard':
            iconName =
              Platform.OS === 'ios'
                ? `ios-home${focused ? '' : '-outline'}`
                : 'home';
            break;
          case 'Worklogs':
            iconName = Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'account-settings-variant';
            break;
          case 'Leaves':
            iconName =
              Platform.OS === 'ios' ? `ios-calendar-clock${focused ? '' : '-outline'}` : 'calendar-clock';
              break;
          case 'More':
            iconName =
            Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'view-grid';
        }
        return (
          <MaterialCommunityIcons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
      
    }),
    tabBarOptions: {
      activeTintColor: '#33b0df',
      showLabel: false,
      labelStyle: {
        fontSize: 12,
      },
      style: {
        backgroundColor: 'white',
        elevation: 20,
        borderTopColor: 'transparent'
      },
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
  }
);
