import React from 'react';
import Touchable from 'react-native-platform-touchable';
import { StyleSheet, Image, Text, View, Dimensions, ScrollView } from 'react-native';

const width = Dimensions.get('window').width

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <View style={styles.container}>
        <Touchable style={styles.widgets}>
            <View style={styles.widgetsContainer}>
              <Image
                source={require('../assets/images/todayworklog.png')}
               />
              <Text> Today Wroklog </Text>
            </View>
          </Touchable>

          <Touchable style={styles.widgets}>
            <View style={styles.widgetsContainer}>
              <Image
                source={require('../assets/images/pendingworklog.png')}
              />
              <Text> Pending Wroklog </Text>
            </View>
          </Touchable>

          <Touchable style={styles.widgets}>
            <View>
              <Text> Medical Leaves </Text>
            </View>
          </Touchable>

          <Touchable style={styles.widgets}>
            <View>
              <Text> Casual Leaves </Text>
            </View>
          </Touchable>

          <Touchable style={styles.widgets}>
            <View>
              <Image
                source={require('../assets/images/apply_leave.png')}
              />
              <Text> Apply Leaves </Text>
            </View>
          </Touchable>

          <Touchable style={styles.widgets}>
            <View>
              <Image
                source={require('../assets/images/leave_history.png')}
              />
              <Text> Leave History </Text>
            </View>
          </Touchable>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  widgets: {
    width: (width / 2) - 22,
    marginLeft: 15,
    marginTop: 15,
    height: 130,
    padding:20,
    backgroundColor: '#fff',
    elevation: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },

  widgetsContainer: {
    flex: 1,
    padding: 20
  }
});

export default Dashboard;