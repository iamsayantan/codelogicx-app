import React from 'react';
import {ActivityIndicator} from 'react-native';

const Loader = ({color, size}) => {
  return (
    <ActivityIndicator size={size} color={color} />
  );
}

export default Loader;