import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Loader from '../components/Loader';
import Dashboard from '../components/Dashboard';

export default class HomeScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }

    setTimeout(() => {
      this.setState({loading: false});
    }, 2000)
  }
  static navigationOptions = {
    title: 'Dashboard'
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? 
          <Loader color="#33b0df" size={50} /> : 
          <Dashboard />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f2f8',
  },
});
