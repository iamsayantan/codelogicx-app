import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { 
  Card, 
  Button,
  FormLabel,
  FormInput
} from 'react-native-elements'

import { NavigationActions } from 'react-navigation';

import { TextField } from 'react-native-material-textfield';

class LoginScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false
    }

    this.handleLogin = this.handleLogin.bind(this);
    this.resetNavigation = this.resetNavigation.bind(this);
  }

  static navigationOptions = {
    header: null
  };

  handleLogin() {
    this.setState({
      loading: true
    });

    setTimeout(() => {
      this.setState({
        loading: false
      });
      this.resetNavigation('Main');
    });
  }

  // Resets the navigation stack. So when the user navigates to the dashboard
  // after a successfull login, they should not be able to navigate back to
  // the login screen.
  resetNavigation(targetRoute) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: targetRoute }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.loginBox}>
          <View style={{maxHeight: 50}}>
            <Image 
              style={styles.loginLogo} 
              source={require('../../assets/images/codelogicx_logo.png')}
            />
          </View>

          <View
          style={styles.loginCard}
          >
            {/* Login form fields */}
            <View style={styles.loginForm}>
              <TextField
                label='Email'
                value={this.state.email}
                inputContainerStyle={{paddingTop: 24, height: 48, paddingBottom: 6}}
                labelHeight={23}
                //onChangeText={ (phone) => this.setState({ phone }) }
              />

              <TextField
                label='Password'
                value={this.state.password}
                inputContainerStyle={{paddingTop: 24, height: 48, paddingBottom: 6}}
                labelHeight={23}
                secureTextEntry={true}
                //onChangeText={ (phone) => this.setState({ phone }) }
              />
            </View>

            {/* Login Button */}
            <Button
              backgroundColor='#33b0df'
              containerViewStyle={styles.loginButton}
              textStyle={{fontWeight: 'bold', fontSize: 18}}
              title={this.state.loading ? 'Logging in..' : 'Login'}
              onPress={this.handleLogin}
              disabled={this.state.loading ? true : false}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f3f2f8'
  },
  loginBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    //padding: 15,
  },
  loginLogo: {
    flex: 1,
    height: 30,
    width: 200,
    resizeMode: 'center',
  },
  loginCard: {
    minWidth: '80%',
    margin: 15,
    padding: 0,
    backgroundColor: '#ffffff',
    elevation: 8,
    alignSelf: 'stretch',
  },
  loginForm: {
    padding: 10
  },
  loginButton: {
    borderRadius: 0,
    marginLeft:0,
    marginRight: 0,
    alignSelf: 'stretch',
  }
});

export default LoginScreen;